frm = 0;
fpl = 0;

function inicio() {
	_ww = $(window).width();
	$('#topo').css({'top': '-70px', 'display': 'block'});
	$('#topo').animate({top: 0}, 1200);
	$('#pag0').css({'opacity': '0', 'display': 'block'});
	$('#bol0').css({'opacity': '0', 'margin-left': '-' + (_ww * 0.5) + 'px', 'display': 'block'});
	$('#pag0').show();
	$('#pag0').animate({opacity: 1}, 800);
	$('#bol0').animate({marginLeft: 0, opacity: 1}, 1200, function(){ $('#pag1').show(); $('#pag2').show(); $('#pag3').show(); $('#pag4').show(); $('#pag5').show(); });
}

function pagina(n) {
	$('html,body').animate({scrollTop: $('#pag'+n).offset().top - 40}, 800);
}

function menumobile() {
	if ( $('#mobile').css('display') == 'none' ) {
		$('#mobile').css({'right': '-400px', 'display': 'block'});
		$('#mobile').animate({right: 0}, 600);
	} else {
		$('#mobile').animate({right: -400}, 600, function(){ $('#mobile').hide(); });
	}
}

function form(n) {
	_ww = $(window).width();
	if ( n >= 0 ) {
		$('#form').css({'left': '-'+_ww+'px', 'display': 'block'});
		$('#form').animate({left: 0}, 800);
		if ( n > 0 ) formplano(n);
		frm = 1;
	} else {
		$('#form').animate({left: -_ww}, 800, function(){ $('#form').hide(); });
		frm = 0;
	}
}

function formplano(n) {
	if ( fpl > 0 ) $('#fpl'+fpl).removeClass('ativo');
	$('#fpl'+n).addClass('ativo');
	$('#frmplano').val(n);
	fpl = n;
}

function enviar() {
	if ( ( document.frmcontato.plano.value == "" ) || ( document.frmcontato.nome.value == "" ) || ( document.frmcontato.email.value == "" ) || ( document.frmcontato.telefone.value == "" ) || ( document.frmcontato.cidade.value == "" ) ) {
		alert('Selecione um plano e preencha todos os campos antes de enviar suas informações!');
	} else {
		document.frmcontato.submit();
	}
}
