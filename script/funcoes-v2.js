andando = 0;
pag = 0;
tip = 0;
pla = 0;
pl1 = 0;
pl2 = 0;
pl3 = 0;
pl4 = 0;
pl5 = 0;
frm = 0;
fpl = 0;

$(document).ready(function(){
	doResize();
	$(window).on('resize', doResize);
});

$(window).on('DOMMouseScroll mousewheel', function (e) {
	if ( !frm ) {
		if ( e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0 ) {
			if ( $(window).width() > 800 ) {
				if ( pag < 6 ) {
					pagina(pag+1);
				}
			} else {
				if ( pag < 5 ) {
					pagina(pag+1);
				}
			}
		} else {
			if ( pag > 0 ) {
				if ( pag == 6 ) {
					if ( $('#bol6').scrollTop() == 0 ) {
						pagina(pag-1);
					}
				} else {
					pagina(pag-1);
				}
			}
		}
	}
});

function doResize() {
	_w = $(window).width();
	_h = $(window).height();
	if ( _h > _w ) {
		$('.pagina0 .bola, .pagina1 .bola, .pagina2 .bola, .pagina3 .bola, .pagina4 .bola, .pagina5 .bola').addClass('invert');
		$('.pagina1 .seta, .pagina2 .seta, .pagina3 .seta, .pagina4 .seta, .pagina5 .seta').css({'display': 'none'});
		if ( _w <= 750 ) {
			$('.pagina0 .fundo, .pagina1 .fundo, .pagina2 .fundo, .pagina3 .fundo, .pagina4 .fundo, .pagina5 .fundo').addClass('invertm');
		}
	} else {
		$('.pagina0 .bola, .pagina1 .bola, .pagina2 .bola, .pagina3 .bola, .pagina4 .bola, .pagina5 .bola').removeClass('invert');
		$('.pagina1 .seta, .pagina2 .seta, .pagina3 .seta, .pagina4 .seta, .pagina5 .seta').css({'display': ''});
	}
}

function inicio() {
	_ww = $(window).width();
	$('#topo').css({'top':'-70px','display':'block'});
	$('#topo').animate({top: 0}, 1200);
	$('#img0').css({'opacity':'0','display':'block'});
	$('#marcas').css({'left':'-20px','display':'block'});
	$('#bol0').css({'opacity':'0','left':'-' + (_ww * 0.5) + 'px','display':'block'});
	$('#pag0').show();
	$('#img0').animate({opacity: 1}, 800);
	$('#marcas').animate({left: (_ww * 0.02)}, 1800);
	$('#bol0').animate({left: 0, opacity: 1}, 1200);
	mover_seta();
}

function pagina(n) {
	_ww = $(window).width();
	_wh = $(window).height();
	if ( !andando ) {
		if ( pag != n ) {
			andando = 1;
			tmp = pag;
			$('#mrc'+tmp).removeClass('ativo');
			$('#mrc'+n).addClass('ativo');
			if ( pag < n ) {
				$('#img'+tmp).animate({top: -_wh}, 800, function(){ $('#pag'+tmp).hide(); });
				$('#img'+n).css({'top':_wh+'px','display':'block'});
			} else {
				$('#img'+tmp).animate({top: _wh}, 800, function(){ $('#pag'+tmp).hide(); });
				$('#img'+n).css({'top':'-'+_wh+'px','display':'block'});
			}
			$('#bol'+n).css({'opacity':'0','left':'-' + (_ww * 0.5) + 'px','display':'block'});
			$('#pag'+n).show();
			$('#img'+n).animate({top: 0}, 800);
			$('#bol'+n).animate({left: 0, opacity: 1}, 1200, function(){ pag = n; andando = 0; });
			if ( ( n == 6 ) && ( tip == 0 ) ) setTimeout("marcar_tipo(1);", 2000);
		}
	}
}

function menumobile() {
	if ( $('#mobile').css('display') == 'none' ) {
		$('#mobile').css({'right': '-400px', 'display': 'block'});
		$('#mobile').animate({right: 0}, 600);
	} else {
		$('#mobile').animate({right: -400}, 600, function(){ $('#mobile').hide(); });
	}
}

function mover_seta() {
	$('.seta').animate({bottom: 20}, 200, function(){ $('.seta').animate({bottom: 30}, 800); });
	setTimeout("mover_seta()", 4000);
}

function animar_tipo() {
	$('#seta1').css({'margin-top': '-10px', 'opacity': '0', 'display': 'block'});
	$('#info1').css({'opacity': '0', 'display': 'block'});
	$('#seta1').animate({marginTop: 0, opacity: 1}, 600);
	setTimeout("$('#info1').animate({opacity: 1}, 800)", 400);
}

function marcar_tipo(n) {
	if ( tip != n ) {
		if ( tip == 0 ) {
			animar_planos();
		} else {
			if ( pla > 0 ) {
				for ( i = 1; i <= 5; i++ ) {
					$('#pla'+i).removeClass('ativo');
					$('#plm'+i).removeClass('ativo');
					$('#p'+i+'cx2').animate({opacity: 0}, 400);
					$('#p'+i+'cx3').animate({opacity: 0}, 400);
					$('#p'+i+'cx4').animate({opacity: 0}, 400);
					$('#p'+i+'cx5').animate({opacity: 0}, 400);
					$('#p'+i+'cx6').animate({opacity: 0}, 400);
					$('#p'+i+'cx7').animate({opacity: 0}, 400);
					$('#p'+i+'cx8').animate({opacity: 0}, 400);
				}
			}
		}
		$('#tip1').removeClass('ativo');
		$('#tip2').removeClass('ativo');
		$('#tip'+n).addClass('ativo');
		tip = n;
	}
}

function animar_planos() {
	$('#seta1').animate({opacity: 0}, 600, function(){ $('#seta1').hide(); });
	$('#info1').animate({opacity: 0}, 600, function(){ $('#info1').hide(); });
	$('#p1cx1').removeClass('trans3');
	$('#p1cx1').css({'opacity': '0.3'});
	$('#p2cx1').removeClass('trans3');
	$('#p2cx1').css({'opacity': '0.3'});
	$('#p3cx1').removeClass('trans3');
	$('#p3cx1').css({'opacity': '0.3'});
	$('#p4cx1').removeClass('trans3');
	$('#p4cx1').css({'opacity': '0.3'});
	$('#p5cx1').removeClass('trans3');
	$('#p5cx1').css({'opacity': '0.3'});
	$('#p1cx1').animate({opacity: 1}, 600);
	$('#p2cx1').animate({opacity: 1}, 600);
	$('#p3cx1').animate({opacity: 1}, 600);
	$('#p4cx1').animate({opacity: 1}, 600);
	$('#p5cx1').animate({opacity: 1}, 600);
	$('#seta2').css({'margin-top': '-10px', 'opacity': '0', 'display': 'block'});
	$('#info2').css({'opacity': '0', 'display': 'block'});
	$('#seta2').animate({marginTop: 0, opacity: 1}, 600);
	setTimeout("$('#info2').animate({opacity: 1}, 800)", 400);
}

function esconder_planos() {
	$('#seta2').animate({opacity: 0}, 600, function(){ $('#seta2').hide(); });
	$('#info2').animate({opacity: 0}, 600, function(){ $('#info2').hide(); });
	$('#p0cx2').css({'opacity': '0', 'display': 'block'});
	$('#p0cx3').css({'opacity': '0', 'display': 'block'});
	$('#p0cx4').css({'opacity': '0', 'display': 'block'});
	$('#p0cx5').css({'opacity': '0', 'display': 'block'});
	$('#p0cx6').css({'opacity': '0', 'display': 'block'});
	$('#p0cx7').css({'opacity': '0', 'display': 'block'});
	$('#p0cx8').css({'opacity': '0', 'display': 'block'});
	$('#p0cx9').css({'opacity': '0', 'display': 'block'});
	$('#p0cx10').css({'opacity': '0', 'display': 'block'});
	$('#p0cx2').animate({opacity: 1}, 600);
	$('#p0cx3').animate({opacity: 1}, 600);
	$('#p0cx4').animate({opacity: 1}, 600);
	$('#p0cx5').animate({opacity: 1}, 600);
	$('#p0cx6').animate({opacity: 1}, 600);
	$('#p0cx7').animate({opacity: 1}, 600);
	$('#p0cx8').animate({opacity: 1}, 600);
	$('#p0cx9').animate({opacity: 1}, 600);
	$('#p0cx10').animate({opacity: 1}, 600);
}

function marcar_plano(n) {
	if ( tip > 0 ) {
		if ( pla == 0 ) {
			esconder_planos();
			pla = 1;
		}
		if ( !andando ) {
			andando = 1;
			if ( $('#plm'+n).hasClass('ativo') ) {
				$('#pla'+n).removeClass('ativo');
				$('#plm'+n).removeClass('ativo');
				$('#p'+n+'cx2').animate({opacity: 0}, 400);
				$('#p'+n+'cx3').animate({opacity: 0}, 400);
				$('#p'+n+'cx4').animate({opacity: 0}, 400);
				$('#p'+n+'cx5').animate({opacity: 0}, 400);
				$('#p'+n+'cx6').animate({opacity: 0}, 400);
				$('#p'+n+'cx7').animate({opacity: 0}, 400);
				$('#p'+n+'cx8').animate({opacity: 0}, 400);
				$('#p'+n+'cx9').animate({opacity: 0}, 400);
				$('#p'+n+'cx10').animate({opacity: 0}, 400, function(){ andando = 0; });
			} else {
				$('#pla'+n).addClass('ativo');
				$('#plm'+n).addClass('ativo');
				$('#p'+n+'cx2').css({'opacity': '0'});
				$('#p'+n+'cx3').css({'opacity': '0'});
				$('#p'+n+'cx4').css({'opacity': '0'});
				$('#p'+n+'cx5').css({'opacity': '0'});
				$('#p'+n+'cx6').css({'opacity': '0'});
				$('#p'+n+'cx7').css({'opacity': '0'});
				$('#p'+n+'cx8').css({'opacity': '0'});
				$('#p'+n+'cx9').css({'opacity': '0'});
				$('#p'+n+'cx10').css({'opacity': '0'});
				$('#p'+n+'cx2').animate({opacity: 1}, 400);
				$('#p'+n+'cx3').animate({opacity: 1}, 400);
				$('#p'+n+'cx4').animate({opacity: 1}, 400);
				$('#p'+n+'cx5').animate({opacity: 1}, 400);
				$('#p'+n+'cx6').animate({opacity: 1}, 400);
				$('#p'+n+'cx7').animate({opacity: 1}, 400);
				$('#p'+n+'cx8').animate({opacity: 1}, 400);
				$('#p'+n+'cx9').animate({opacity: 1}, 400);
				$('#p'+n+'cx10').animate({opacity: 1}, 400, function(){ andando = 0; });
			}
		}
	}
}

function form(n) {
	_ww = $(window).width();
	if ( n >= 0 ) {
		$('#form').css({'left': '-'+_ww+'px', 'display': 'block'});
		$('#form').animate({left: 0}, 800);
		if ( n > 0 ) formplano(n);
		$('#nome').focus();
		frm = 1;
	} else {
		$('#form').animate({left: -_ww}, 800, function(){ $('#form').hide(); });
		frm = 0;
	}
}

function formplano(n) {
	if ( fpl > 0 ) $('#fpl'+fpl).removeClass('ativo');
	$('#fpl'+n).addClass('ativo');
	$('#frmplano').val(n);
	fpl = n;
}

function enviar() {
	if ( ( document.frmcontato.plano.value == "" ) || ( document.frmcontato.nome.value == "" ) || ( document.frmcontato.email.value == "" ) || ( document.frmcontato.telefone.value == "" ) || ( document.frmcontato.cidade.value == "" ) ) {
		alert('Selecione um plano e preencha todos os campos antes de enviar suas informações!');
	} else {
		document.frmcontato.submit();
	}
}
